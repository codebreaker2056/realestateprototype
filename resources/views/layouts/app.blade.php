<html>

<head>
    <style>
        .gm-control-active>img {
            box-sizing: content-box;
            display: none;
            left: 50%;
            pointer-events: none;
            position: absolute;
            top: 50%;
            transform: translate(-50%, -50%)
        }

        .gm-control-active>img:nth-child(1) {
            display: block
        }

        .gm-control-active:hover>img:nth-child(1),
        .gm-control-active:active>img:nth-child(1) {
            display: none
        }

        .gm-control-active:hover>img:nth-child(2),
        .gm-control-active:active>img:nth-child(3) {
            display: block
        }
    </style>
    <link type="text/css" rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Google+Sans:400,500,700">
    <style>
        .gm-ui-hover-effect {
            opacity: .6
        }

        .gm-ui-hover-effect:hover {
            opacity: 1
        }
    </style>
    <style>
        .gm-style .gm-style-cc span,
        .gm-style .gm-style-cc a,
        .gm-style .gm-style-mtc div {
            font-size: 10px;
            box-sizing: border-box
        }
    </style>
    <style>
        @media print {

            .gm-style .gmnoprint,
            .gmnoprint {
                display: none
            }
        }

        @media screen {

            .gm-style .gmnoscreen,
            .gmnoscreen {
                display: none
            }
        }
    </style>
    <style>
        .gm-style-pbc {
            transition: opacity ease-in-out;
            background-color: rgba(0, 0, 0, 0.45);
            text-align: center
        }

        .gm-style-pbt {
            font-size: 22px;
            color: white;
            font-family: Roboto, Arial, sans-serif;
            position: relative;
            margin: 0;
            top: 50%;
            -webkit-transform: translateY(-50%);
            -ms-transform: translateY(-50%);
            transform: translateY(-50%)
        }
    </style>
    <style>
        .gm-style img {
            max-width: none;
        }

        .gm-style {
            font: 400 11px Roboto, Arial, sans-serif;
            text-decoration: none;
        }
    </style>
    <style>
        @keyframes beginBrowserAutofill {
            0% {}

            to {}
        }

        @keyframes endBrowserAutofill {
            0% {}

            to {}
        }

        .pac-container {
            background-color: #fff;
            position: absolute !important;
            z-index: 1000;
            border-radius: 2px;
            border-top: 1px solid #d9d9d9;
            font-family: Arial, sans-serif;
            box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            overflow: hidden
        }

        .pac-logo:after {
            content: "";
            padding: 1px 1px 1px 0;
            height: 18px;
            box-sizing: border-box;
            text-align: right;
            display: block;
            background-image: url(https://maps.gstatic.com/mapfiles/api-3/images/powered-by-google-on-white3.png);
            background-position: right;
            background-repeat: no-repeat;
            background-size: 120px 14px
        }

        .hdpi.pac-logo:after {
            background-image: url(https://maps.gstatic.com/mapfiles/api-3/images/powered-by-google-on-white3_hdpi.png)
        }

        .pac-item {
            cursor: default;
            padding: 0 4px;
            text-overflow: ellipsis;
            overflow: hidden;
            white-space: nowrap;
            line-height: 30px;
            text-align: left;
            border-top: 1px solid #e6e6e6;
            font-size: 11px;
            color: #999
        }

        .pac-item:hover {
            background-color: #fafafa
        }

        .pac-item-selected,
        .pac-item-selected:hover {
            background-color: #ebf2fe
        }

        .pac-matched {
            font-weight: 700
        }

        .pac-item-query {
            font-size: 13px;
            padding-right: 3px;
            color: #000
        }

        .pac-icon {
            width: 15px;
            height: 20px;
            margin-right: 7px;
            margin-top: 6px;
            display: inline-block;
            vertical-align: top;
            background-image: url(https://maps.gstatic.com/mapfiles/api-3/images/autocomplete-icons.png);
            background-size: 34px
        }

        .hdpi .pac-icon {
            background-image: url(https://maps.gstatic.com/mapfiles/api-3/images/autocomplete-icons_hdpi.png)
        }

        .pac-icon-search {
            background-position: -1px -1px
        }

        .pac-item-selected .pac-icon-search {
            background-position: -18px -1px
        }

        .pac-icon-marker {
            background-position: -1px -161px
        }

        .pac-item-selected .pac-icon-marker {
            background-position: -18px -161px
        }

        .pac-placeholder {
            color: gray
        }

        .pac-target-input:-webkit-autofill {
            animation-name: beginBrowserAutofill
        }

        .pac-target-input:not(:-webkit-autofill) {
            animation-name: endBrowserAutofill
        }
    </style>

    <!-- Basic Page Needs
================================================== -->
    <title>Real Estate Demo</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
================================================== -->
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/main-color.css" id="colors">

    <script type="text/javascript" charset="UTF-8"
        src="https://maps.googleapis.com/maps-api-v3/api/js/41/8/common.js?key=removedkey&amp;libraries=places&amp;callback=initialize">
    </script>
    <script type="text/javascript" charset="UTF-8"
        src="https://maps.googleapis.com/maps-api-v3/api/js/41/8/util.js?key=removedkey&amp;libraries=places&amp;callback=initialize">
    </script>
    <script type="text/javascript" charset="UTF-8"
        src="https://maps.googleapis.com/maps-api-v3/api/js/41/8/controls.js?key=removedkey&amp;libraries=places&amp;callback=initialize">
    </script>
    <script type="text/javascript" charset="UTF-8"
        src="https://maps.googleapis.com/maps-api-v3/api/js/41/8/places_impl.js?key=removedkey&amp;libraries=places&amp;callback=initialize">
    </script>
    <script type="text/javascript" charset="UTF-8"
        src="https://maps.googleapis.com/maps-api-v3/api/js/41/8/map.js?key=removedkey&amp;libraries=places&amp;callback=initialize">
    </script>
    <script type="text/javascript" charset="UTF-8"
        src="https://maps.googleapis.com/maps-api-v3/api/js/41/8/overlay.js?key=removedkey&amp;libraries=places&amp;callback=initialize">
    </script>
    <script type="text/javascript" charset="UTF-8"
        src="https://maps.googleapis.com/maps-api-v3/api/js/41/8/stats.js?key=removedkey&amp;libraries=places&amp;callback=initialize">
    </script>
    <script type="text/javascript" charset="UTF-8"
        src="https://maps.googleapis.com/maps-api-v3/api/js/41/8/onion.js?key=removedkey&amp;libraries=places&amp;callback=initialize">
    </script>
</head>

<body data-gr-c-s-loaded="true">
    {{-- <nav class="mmenu-init mm-menu mm-offcanvas" id="mm-0" aria-hidden="true">
        <div class="mm-panels">
            <div class="mm-panel mm-hasnavbar mm-opened" id="mm-1">
                <div class="mm-navbar"><a class="mm-title">Menu</a></div>
                <ul class="mm-listview">

                    <li><em class="mm-counter">6</em><a class="mm-next mm-fullsubopen" href="#mm-2" aria-owns="mm-2"
                            aria-haspopup="true"><span class="mm-sronly">Open submenu (Home)</span></a><a
                            class="current" href="#">Home</a>

                    </li>

                    <li><em class="mm-counter">5</em><a class="mm-next mm-fullsubopen" href="#mm-3" aria-owns="mm-3"
                            aria-haspopup="true"><span class="mm-sronly">Open submenu (Listings)</span></a><a
                            href="#">Listings</a>

                    </li>

                    <li><em class="mm-counter">10</em><a class="mm-next mm-fullsubopen" href="#mm-9" aria-owns="mm-9"
                            aria-haspopup="true"><span class="mm-sronly">Open submenu (User Panel)</span></a><a
                            href="#">User Panel</a>

                    </li>

                    <li><em class="mm-counter">15</em><a class="mm-next mm-fullsubopen" href="#mm-10" aria-owns="mm-10"
                            aria-haspopup="true"><span class="mm-sronly">Open submenu (Pages)</span></a><a
                            href="#">Pages</a>

                    </li>

                </ul>
            </div>
            <div class="mm-panel mm-hidden mm-hasnavbar" id="mm-2" aria-hidden="true">
                <div class="mm-navbar"><a class="mm-btn mm-prev" href="#mm-1" aria-owns="mm-1"
                        aria-haspopup="true"><span class="mm-sronly">Close submenu (Home)</span></a><a class="mm-title"
                        href="#mm-1" aria-hidden="true">Home</a></div>
                <ul class="mm-listview">
                    <li><a href="index.html">Home 1</a></li>
                    <li><a href="index-2-airbnb.html">Home 2 (Airbnb)</a></li>
                    <li><a href="index-3.html">Home 3</a></li>
                    <li><a href="index-4.html">Home 4</a></li>
                    <li><a href="index-5.html">Home 5</a></li>
                    <li><a href="index-6.html">Home 6</a></li>
                </ul>
            </div>
            <div class="mm-panel mm-hidden mm-hasnavbar" id="mm-3" aria-hidden="true">
                <div class="mm-navbar"><a class="mm-btn mm-prev" href="#mm-1" aria-owns="mm-1"
                        aria-haspopup="true"><span class="mm-sronly">Close submenu (Listings)</span></a><a
                        class="mm-title" href="#mm-1" aria-hidden="true">Listings</a></div>
                <ul class="mm-listview">
                    <li><em class="mm-counter">3</em><a class="mm-next mm-fullsubopen" href="#mm-4" aria-owns="mm-4"
                            aria-haspopup="true"><span class="mm-sronly">Open submenu (List Layout)</span></a><a
                            href="#">List Layout</a>

                    </li>
                    <li><em class="mm-counter">4</em><a class="mm-next mm-fullsubopen" href="#mm-5" aria-owns="mm-5"
                            aria-haspopup="true"><span class="mm-sronly">Open submenu (Grid Layout)</span></a><a
                            href="#">Grid Layout</a>

                    </li>
                    <li><em class="mm-counter">3</em><a class="mm-next mm-fullsubopen" href="#mm-6" aria-owns="mm-6"
                            aria-haspopup="true"><span class="mm-sronly">Open submenu (Half Screen Map)</span></a><a
                            href="#">Half Screen Map</a>

                    </li>
                    <li><em class="mm-counter">3</em><a class="mm-next mm-fullsubopen" href="#mm-7" aria-owns="mm-7"
                            aria-haspopup="true"><span class="mm-sronly">Open submenu (Single Listings)</span></a><a
                            href="#">Single Listings</a>

                    </li>
                    <li><em class="mm-counter">6</em><a class="mm-next mm-fullsubopen" href="#mm-8" aria-owns="mm-8"
                            aria-haspopup="true"><span class="mm-sronly">Open submenu (Open Street Map)</span></a><a
                            href="#">Open Street Map</a>

                    </li>
                </ul>
            </div>
            <div class="mm-panel mm-hidden mm-hasnavbar" id="mm-4" aria-hidden="true">
                <div class="mm-navbar"><a class="mm-btn mm-prev" href="#mm-3" aria-owns="mm-3"
                        aria-haspopup="true"><span class="mm-sronly">Close submenu (List Layout)</span></a><a
                        class="mm-title" href="#mm-3" aria-hidden="true">List Layout</a></div>
                <ul class="mm-listview">
                    <li><a href="listings-list-with-sidebar.html">With Sidebar</a></li>
                    <li><a href="listings-list-full-width.html">Full Width</a></li>
                    <li><a href="listings-list-full-width-with-map.html">Full Width + Map</a></li>
                </ul>
            </div>
            <div class="mm-panel mm-hidden mm-hasnavbar" id="mm-5" aria-hidden="true">
                <div class="mm-navbar"><a class="mm-btn mm-prev" href="#mm-3" aria-owns="mm-3"
                        aria-haspopup="true"><span class="mm-sronly">Close submenu (Grid Layout)</span></a><a
                        class="mm-title" href="#mm-3" aria-hidden="true">Grid Layout</a></div>
                <ul class="mm-listview">
                    <li><a href="listings-grid-with-sidebar-1.html">With Sidebar 1</a></li>
                    <li><a href="listings-grid-with-sidebar-2.html">With Sidebar 2</a></li>
                    <li><a href="listings-grid-full-width.html">Full Width</a></li>
                    <li><a href="listings-grid-full-width-with-map.html">Full Width + Map</a></li>
                </ul>
            </div>
            <div class="mm-panel mm-hidden mm-hasnavbar" id="mm-6" aria-hidden="true">
                <div class="mm-navbar"><a class="mm-btn mm-prev" href="#mm-3" aria-owns="mm-3"
                        aria-haspopup="true"><span class="mm-sronly">Close submenu (Half Screen Map)</span></a><a
                        class="mm-title" href="#mm-3" aria-hidden="true">Half Screen Map</a></div>
                <ul class="mm-listview">
                    <li><a href="listings-half-screen-map-list.html">List Layout</a></li>
                    <li><a href="listings-half-screen-map-grid-1.html">Grid Layout 1</a></li>
                    <li><a href="listings-half-screen-map-grid-2.html">Grid Layout 2</a></li>
                </ul>
            </div>
            <div class="mm-panel mm-hidden mm-hasnavbar" id="mm-7" aria-hidden="true">
                <div class="mm-navbar"><a class="mm-btn mm-prev" href="#mm-3" aria-owns="mm-3"
                        aria-haspopup="true"><span class="mm-sronly">Close submenu (Single Listings)</span></a><a
                        class="mm-title" href="#mm-3" aria-hidden="true">Single Listings</a></div>
                <ul class="mm-listview">
                    <li><a href="listings-single-page.html">Single Listing 1</a></li>
                    <li><a href="listings-single-page-2.html">Single Listing 2</a></li>
                    <li><a href="listings-single-page-3.html">Single Listing 3</a></li>
                </ul>
            </div>
            <div class="mm-panel mm-hidden mm-hasnavbar" id="mm-8" aria-hidden="true">
                <div class="mm-navbar"><a class="mm-btn mm-prev" href="#mm-3" aria-owns="mm-3"
                        aria-haspopup="true"><span class="mm-sronly">Close submenu (Open Street Map)</span></a><a
                        class="mm-title" href="#mm-3" aria-hidden="true">Open Street Map</a></div>
                <ul class="mm-listview">
                    <li><a href="listings-half-screen-map-list-OpenStreetMap.html">Half Screen Map List Layout</a></li>
                    <li><a href="listings-half-screen-map-grid-1-OpenStreetMap.html">Half Screen Map Grid Layout 1</a>
                    </li>
                    <li><a href="listings-half-screen-map-grid-2-OpenStreetMap.html">Half Screen Map Grid Layout 2</a>
                    </li>
                    <li><a href="listings-list-full-width-with-map-OpenStreetMap.html">Full Width List</a></li>
                    <li><a href="listings-grid-full-width-with-map-OpenStreetMap.html">Full Width Grid</a></li>
                    <li><a href="listings-single-page-OpenStreetMap.html">Single Listing</a></li>
                </ul>
            </div>
            <div class="mm-panel mm-hidden mm-hasnavbar" id="mm-9" aria-hidden="true">
                <div class="mm-navbar"><a class="mm-btn mm-prev" href="#mm-1" aria-owns="mm-1"
                        aria-haspopup="true"><span class="mm-sronly">Close submenu (User Panel)</span></a><a
                        class="mm-title" href="#mm-1" aria-hidden="true">User Panel</a></div>
                <ul class="mm-listview">
                    <li><a href="dashboard.html">Dashboard</a></li>
                    <li><a href="dashboard-messages.html">Messages</a></li>
                    <li><a href="dashboard-bookings.html">Bookings</a></li>
                    <li><a href="dashboard-wallet.html">Wallet</a></li>
                    <li><a href="dashboard-my-listings.html">My Listings</a></li>
                    <li><a href="dashboard-reviews.html">Reviews</a></li>
                    <li><a href="dashboard-bookmarks.html">Bookmarks</a></li>
                    <li><a href="dashboard-add-listing.html">Add Listing</a></li>
                    <li><a href="dashboard-my-profile.html">My Profile</a></li>
                    <li><a href="dashboard-invoice.html">Invoice</a></li>
                </ul>
            </div>
            <div class="mobile-styles three-columns mm-panel mm-hidden mm-hasnavbar" id="mm-10" aria-hidden="true">
                <div class="mm-navbar"><a class="mm-btn mm-prev" href="#mm-1" aria-owns="mm-1"
                        aria-haspopup="true"><span class="mm-sronly">Close submenu (Pages)</span></a><a class="mm-title"
                        href="#mm-1" aria-hidden="true">Pages</a></div>


                <ul class="mm-listview">
                    <li class="mega-menu-headline">Pages #1</li>
                    <li><a href="pages-user-profile.html"><i class="sl sl-icon-user"></i> User Profile</a></li>
                    <li><a href="pages-booking.html"><i class="sl sl-icon-check"></i> Booking Page</a></li>
                    <li><a href="pages-add-listing.html"><i class="sl sl-icon-plus"></i> Add Listing</a></li>
                    <li><a href="pages-blog.html"><i class="sl sl-icon-docs"></i> Blog</a></li>
                </ul>



                <ul class="mm-listview">
                    <li class="mega-menu-headline">Pages #2</li>
                    <li><a href="pages-contact.html"><i class="sl sl-icon-envelope-open"></i> Contact</a></li>
                    <li><a href="pages-coming-soon.html"><i class="sl sl-icon-hourglass"></i> Coming Soon</a></li>
                    <li><a href="pages-404.html"><i class="sl sl-icon-close"></i> 404 Page</a></li>
                    <li><a href="pages-masonry-filtering.html"><i class="sl sl-icon-equalizer"></i> Masonry
                            Filtering</a></li>
                </ul>



                <ul class="mm-listview">
                    <li class="mega-menu-headline">Other</li>
                    <li><a href="pages-elements.html"><i class="sl sl-icon-settings"></i> Elements</a></li>
                    <li><a href="pages-pricing-tables.html"><i class="sl sl-icon-tag"></i> Pricing Tables</a></li>
                    <li><a href="pages-typography.html"><i class="sl sl-icon-pencil"></i> Typography</a></li>
                    <li><a href="pages-icons.html"><i class="sl sl-icon-diamond"></i> Icons</a></li>
                </ul>


            </div>
        </div>

    </nav> --}}

    <!-- Wrapper -->
    <div id="wrapper" class="mm-page mm-slideout">
        <!-- Header Container
================================================== -->
        <header id="header-container">
            <!-- Header -->
            <div id="header">
                <div class="container">

                    <!-- Left Side Content -->
                    <div class="left-side">

                        <!-- Logo -->
                        <div id="logo">
                            <a href="index.html"><img src="images/logo.png" alt=""></a>
                        </div>

                        <!-- Mobile Navigation -->
                        <div class="mmenu-trigger">
                            <button class="hamburger hamburger--collapse" type="button">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>

                        <!-- Main Navigation -->
                        {{-- <nav id="navigation" class="style-1">
                            <ul id="responsive">

                                <li><a class="current" href="#">Home</a>
                                    <ul>
                                        <li><a href="index.html">Home 1</a></li>
                                        <li><a href="index-2-airbnb.html">Home 2 (Airbnb)</a></li>
                                        <li><a href="index-3.html">Home 3</a></li>
                                        <li><a href="index-4.html">Home 4</a></li>
                                        <li><a href="index-5.html">Home 5</a></li>
                                        <li><a href="index-6.html">Home 6</a></li>
                                    </ul>
                                </li>

                                <li><a href="#">Listings</a>
                                    <ul>
                                        <li><a href="#">List Layout</a>
                                            <ul>
                                                <li><a href="listings-list-with-sidebar.html">With Sidebar</a></li>
                                                <li><a href="listings-list-full-width.html">Full Width</a></li>
                                                <li><a href="listings-list-full-width-with-map.html">Full Width +
                                                        Map</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#">Grid Layout</a>
                                            <ul>
                                                <li><a href="listings-grid-with-sidebar-1.html">With Sidebar 1</a></li>
                                                <li><a href="listings-grid-with-sidebar-2.html">With Sidebar 2</a></li>
                                                <li><a href="listings-grid-full-width.html">Full Width</a></li>
                                                <li><a href="listings-grid-full-width-with-map.html">Full Width +
                                                        Map</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#">Half Screen Map</a>
                                            <ul>
                                                <li><a href="listings-half-screen-map-list.html">List Layout</a></li>
                                                <li><a href="listings-half-screen-map-grid-1.html">Grid Layout 1</a>
                                                </li>
                                                <li><a href="listings-half-screen-map-grid-2.html">Grid Layout 2</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a href="#">Single Listings</a>
                                            <ul>
                                                <li><a href="listings-single-page.html">Single Listing 1</a></li>
                                                <li><a href="listings-single-page-2.html">Single Listing 2</a></li>
                                                <li><a href="listings-single-page-3.html">Single Listing 3</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#">Open Street Map</a>
                                            <ul>
                                                <li><a href="listings-half-screen-map-list-OpenStreetMap.html">Half
                                                        Screen Map List Layout</a></li>
                                                <li><a href="listings-half-screen-map-grid-1-OpenStreetMap.html">Half
                                                        Screen Map Grid Layout 1</a></li>
                                                <li><a href="listings-half-screen-map-grid-2-OpenStreetMap.html">Half
                                                        Screen Map Grid Layout 2</a></li>
                                                <li><a href="listings-list-full-width-with-map-OpenStreetMap.html">Full
                                                        Width List</a></li>
                                                <li><a href="listings-grid-full-width-with-map-OpenStreetMap.html">Full
                                                        Width Grid</a></li>
                                                <li><a href="listings-single-page-OpenStreetMap.html">Single Listing</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>

                                <li><a href="#">User Panel</a>
                                    <ul>
                                        <li><a href="dashboard.html">Dashboard</a></li>
                                        <li><a href="dashboard-messages.html">Messages</a></li>
                                        <li><a href="dashboard-bookings.html">Bookings</a></li>
                                        <li><a href="dashboard-wallet.html">Wallet</a></li>
                                        <li><a href="dashboard-my-listings.html">My Listings</a></li>
                                        <li><a href="dashboard-reviews.html">Reviews</a></li>
                                        <li><a href="dashboard-bookmarks.html">Bookmarks</a></li>
                                        <li><a href="dashboard-add-listing.html">Add Listing</a></li>
                                        <li><a href="dashboard-my-profile.html">My Profile</a></li>
                                        <li><a href="dashboard-invoice.html">Invoice</a></li>
                                    </ul>
                                </li>

                                <li><a href="#">Pages</a>
                                    <div class="mega-menu mobile-styles three-columns">

                                        <div class="mega-menu-section">
                                            <ul>
                                                <li class="mega-menu-headline">Pages #1</li>
                                                <li><a href="pages-user-profile.html"><i class="sl sl-icon-user"></i>
                                                        User Profile</a></li>
                                                <li><a href="pages-booking.html"><i class="sl sl-icon-check"></i>
                                                        Booking Page</a></li>
                                                <li><a href="pages-add-listing.html"><i class="sl sl-icon-plus"></i> Add
                                                        Listing</a></li>
                                                <li><a href="pages-blog.html"><i class="sl sl-icon-docs"></i> Blog</a>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="mega-menu-section">
                                            <ul>
                                                <li class="mega-menu-headline">Pages #2</li>
                                                <li><a href="pages-contact.html"><i
                                                            class="sl sl-icon-envelope-open"></i>
                                                        Contact</a></li>
                                                <li><a href="pages-coming-soon.html"><i
                                                            class="sl sl-icon-hourglass"></i> Coming
                                                        Soon</a></li>
                                                <li><a href="pages-404.html"><i class="sl sl-icon-close"></i> 404
                                                        Page</a></li>
                                                <li><a href="pages-masonry-filtering.html"><i
                                                            class="sl sl-icon-equalizer"></i>
                                                        Masonry Filtering</a></li>
                                            </ul>
                                        </div>

                                        <div class="mega-menu-section">
                                            <ul>
                                                <li class="mega-menu-headline">Other</li>
                                                <li><a href="pages-elements.html"><i class="sl sl-icon-settings"></i>
                                                        Elements</a></li>
                                                <li><a href="pages-pricing-tables.html"><i class="sl sl-icon-tag"></i>
                                                        Pricing Tables</a></li>
                                                <li><a href="pages-typography.html"><i class="sl sl-icon-pencil"></i>
                                                        Typography</a></li>
                                                <li><a href="pages-icons.html"><i class="sl sl-icon-diamond"></i>
                                                        Icons</a></li>
                                            </ul>
                                        </div>

                                    </div>
                                </li>

                            </ul>
                        </nav> --}}
                        <div class="clearfix"></div>
                        <!-- Main Navigation / End -->

                    </div>
                    <!-- Left Side Content / End -->


                    <!-- Right Side Content / End -->
                    <div class="right-side">
                        <div class="header-widget">
                            {{-- <a href="#sign-in-dialog" class="sign-in popup-with-zoom-anim"><i
                                    class="sl sl-icon-login"></i> Sign
                                In</a> --}}
                            <a href="{{ route('create') }}" class="button border with-icon">Add Listing <i
                                    class="sl sl-icon-plus"></i></a>
                        </div>
                    </div>
                    <!-- Right Side Content / End -->

                    <!-- Sign In Popup -->
                    <div id="sign-in-dialog" class="zoom-anim-dialog mfp-hide">
                        <div class="small-dialog-header">
                            <h3>Sign In</h3>
                        </div>

                        <!--Tabs -->
                        <div class="sign-in-form style-1">

                            <ul class="tabs-nav">
                                <li class="active"><a href="#tab1">Log In</a></li>
                                <li><a href="#tab2">Register</a></li>
                            </ul>

                            <div class="tabs-container alt">
                                <!-- Login -->
                                <div class="tab-content" id="tab1" style="">
                                    <form method="post" class="login">
                                        <p class="form-row form-row-wide">
                                            <label for="username">Username:
                                                <i class="im im-icon-Male"></i>
                                                <input type="text" class="input-text" name="username" id="username"
                                                    value="">
                                            </label>
                                        </p>

                                        <p class="form-row form-row-wide">
                                            <label for="password">Password:
                                                <i class="im im-icon-Lock-2"></i>
                                                <input class="input-text" type="password" name="password" id="password">
                                            </label>
                                            <span class="lost_password">
                                                <a href="#">Lost Your Password?</a>
                                            </span>
                                        </p>

                                        <div class="form-row">
                                            <input type="submit" class="button border margin-top-5" name="login"
                                                value="Login">
                                            <div class="checkboxes margin-top-10">
                                                <input id="remember-me" type="checkbox" name="check">
                                                <label for="remember-me">Remember Me</label>
                                            </div>
                                        </div>

                                    </form>
                                </div>

                                <!-- Register -->
                                <div class="tab-content" id="tab2" style="display: none;">

                                    <form method="post" class="register">

                                        <p class="form-row form-row-wide">
                                            <label for="username2">Username:
                                                <i class="im im-icon-Male"></i>
                                                <input type="text" class="input-text" name="username" id="username2"
                                                    value="">
                                            </label>
                                        </p>

                                        <p class="form-row form-row-wide">
                                            <label for="email2">Email Address:
                                                <i class="im im-icon-Mail"></i>
                                                <input type="text" class="input-text" name="email" id="email2" value="">
                                            </label>
                                        </p>

                                        <p class="form-row form-row-wide">
                                            <label for="password1">Password:
                                                <i class="im im-icon-Lock-2"></i>
                                                <input class="input-text" type="password" name="password1"
                                                    id="password1">
                                            </label>
                                        </p>

                                        <p class="form-row form-row-wide">
                                            <label for="password2">Repeat Password:
                                                <i class="im im-icon-Lock-2"></i>
                                                <input class="input-text" type="password" name="password2"
                                                    id="password2">
                                            </label>
                                        </p>

                                        <input type="submit" class="button border fw margin-top-10" name="register"
                                            value="Register">

                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- Sign In Popup / End -->

                </div>
            </div>
            <div id="header" class="cloned sticky">
                <div class="container">
                    <!-- Left Side Content -->
                    <div class="left-side">
                        <!-- Logo -->
                        <div id="logo">
                            <a href="index.html"><img src="images/logo.png" alt=""></a>
                        </div>

                        <!-- Mobile Navigation -->
                        <div class="mmenu-trigger">
                            <button class="hamburger hamburger--collapse" type="button">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>

                        <!-- Main Navigation -->
                        {{-- <nav id="navigation" class="style-1">
                            <ul id="responsive">

                                <li><a class="current" href="#">Home</a>
                                    <ul>
                                        <li><a href="index.html">Home 1</a></li>
                                        <li><a href="index-2-airbnb.html">Home 2 (Airbnb)</a></li>
                                        <li><a href="index-3.html">Home 3</a></li>
                                        <li><a href="index-4.html">Home 4</a></li>
                                        <li><a href="index-5.html">Home 5</a></li>
                                        <li><a href="index-6.html">Home 6</a></li>
                                    </ul>
                                </li>

                                <li><a href="#">Listings</a>
                                    <ul>
                                        <li><a href="#">List Layout</a>
                                            <ul>
                                                <li><a href="listings-list-with-sidebar.html">With Sidebar</a></li>
                                                <li><a href="listings-list-full-width.html">Full Width</a></li>
                                                <li><a href="listings-list-full-width-with-map.html">Full Width +
                                                        Map</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#">Grid Layout</a>
                                            <ul>
                                                <li><a href="listings-grid-with-sidebar-1.html">With Sidebar 1</a></li>
                                                <li><a href="listings-grid-with-sidebar-2.html">With Sidebar 2</a></li>
                                                <li><a href="listings-grid-full-width.html">Full Width</a></li>
                                                <li><a href="listings-grid-full-width-with-map.html">Full Width +
                                                        Map</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#">Half Screen Map</a>
                                            <ul>
                                                <li><a href="listings-half-screen-map-list.html">List Layout</a></li>
                                                <li><a href="listings-half-screen-map-grid-1.html">Grid Layout 1</a>
                                                </li>
                                                <li><a href="listings-half-screen-map-grid-2.html">Grid Layout 2</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a href="#">Single Listings</a>
                                            <ul>
                                                <li><a href="listings-single-page.html">Single Listing 1</a></li>
                                                <li><a href="listings-single-page-2.html">Single Listing 2</a></li>
                                                <li><a href="listings-single-page-3.html">Single Listing 3</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#">Open Street Map</a>
                                            <ul>
                                                <li><a href="listings-half-screen-map-list-OpenStreetMap.html">Half
                                                        Screen Map List Layout</a></li>
                                                <li><a href="listings-half-screen-map-grid-1-OpenStreetMap.html">Half
                                                        Screen Map Grid Layout 1</a></li>
                                                <li><a href="listings-half-screen-map-grid-2-OpenStreetMap.html">Half
                                                        Screen Map Grid Layout 2</a></li>
                                                <li><a href="listings-list-full-width-with-map-OpenStreetMap.html">Full
                                                        Width List</a></li>
                                                <li><a href="listings-grid-full-width-with-map-OpenStreetMap.html">Full
                                                        Width Grid</a></li>
                                                <li><a href="listings-single-page-OpenStreetMap.html">Single Listing</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>

                                <li><a href="#">User Panel</a>
                                    <ul>
                                        <li><a href="dashboard.html">Dashboard</a></li>
                                        <li><a href="dashboard-messages.html">Messages</a></li>
                                        <li><a href="dashboard-bookings.html">Bookings</a></li>
                                        <li><a href="dashboard-wallet.html">Wallet</a></li>
                                        <li><a href="dashboard-my-listings.html">My Listings</a></li>
                                        <li><a href="dashboard-reviews.html">Reviews</a></li>
                                        <li><a href="dashboard-bookmarks.html">Bookmarks</a></li>
                                        <li><a href="dashboard-add-listing.html">Add Listing</a></li>
                                        <li><a href="dashboard-my-profile.html">My Profile</a></li>
                                        <li><a href="dashboard-invoice.html">Invoice</a></li>
                                    </ul>
                                </li>

                                <li><a href="#">Pages</a>
                                    <div class="mega-menu mobile-styles three-columns">

                                        <div class="mega-menu-section">
                                            <ul>
                                                <li class="mega-menu-headline">Pages #1</li>
                                                <li><a href="pages-user-profile.html"><i class="sl sl-icon-user"></i>
                                                        User Profile</a></li>
                                                <li><a href="pages-booking.html"><i class="sl sl-icon-check"></i>
                                                        Booking Page</a></li>
                                                <li><a href="pages-add-listing.html"><i class="sl sl-icon-plus"></i> Add
                                                        Listing</a></li>
                                                <li><a href="pages-blog.html"><i class="sl sl-icon-docs"></i> Blog</a>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="mega-menu-section">
                                            <ul>
                                                <li class="mega-menu-headline">Pages #2</li>
                                                <li><a href="pages-contact.html"><i
                                                            class="sl sl-icon-envelope-open"></i>
                                                        Contact</a></li>
                                                <li><a href="pages-coming-soon.html"><i
                                                            class="sl sl-icon-hourglass"></i> Coming
                                                        Soon</a></li>
                                                <li><a href="pages-404.html"><i class="sl sl-icon-close"></i> 404
                                                        Page</a></li>
                                                <li><a href="pages-masonry-filtering.html"><i
                                                            class="sl sl-icon-equalizer"></i>
                                                        Masonry Filtering</a></li>
                                            </ul>
                                        </div>

                                        <div class="mega-menu-section">
                                            <ul>
                                                <li class="mega-menu-headline">Other</li>
                                                <li><a href="pages-elements.html"><i class="sl sl-icon-settings"></i>
                                                        Elements</a></li>
                                                <li><a href="pages-pricing-tables.html"><i class="sl sl-icon-tag"></i>
                                                        Pricing Tables</a></li>
                                                <li><a href="pages-typography.html"><i class="sl sl-icon-pencil"></i>
                                                        Typography</a></li>
                                                <li><a href="pages-icons.html"><i class="sl sl-icon-diamond"></i>
                                                        Icons</a></li>
                                            </ul>
                                        </div>

                                    </div>
                                </li>

                            </ul>
                        </nav> --}}
                        <div class="clearfix"></div>
                        <!-- Main Navigation / End -->

                    </div>
                    <!-- Left Side Content / End -->


                    <!-- Right Side Content / End -->
                    <div class="right-side">
                        <div class="header-widget">
                            {{-- <a href="#sign-in-dialog" class="sign-in popup-with-zoom-anim"><i
                                    class="sl sl-icon-login"></i> Sign
                                In</a> --}}
                            <a href="{{ route('create') }}" class="button border with-icon">Add Listing <i
                                    class="sl sl-icon-plus"></i></a>
                        </div>
                    </div>
                    <!-- Right Side Content / End -->

                    <!-- Sign In Popup -->

                    <!-- Sign In Popup / End -->

                </div>
            </div>
            <!-- Header / End -->

        </header>

        <div class="clearfix"></div>
        <!-- Header Container / End -->

        @yield('content')

        <!-- Footer
================================================== -->
        <div id="footer">
            <!-- Main -->
            <div class="container">
                <div class="row">
                    <div class="col-md-5 col-sm-6">
                        <img class="footer-logo" src="images/logo.png" alt="">
                        <br><br>
                        <p>Morbi convallis bibendum urna ut viverra. Maecenas quis consequat libero, a feugiat eros.
                            Nunc ut lacinia tortor morbi ultricies laoreet ullamcorper phasellus semper.</p>
                    </div>

                    <div class="col-md-4 col-sm-6 ">
                        <h4>Helpful Links</h4>
                        <ul class="footer-links">
                            <li><a href="#">Login</a></li>
                            <li><a href="#">Sign Up</a></li>
                            <li><a href="#">My Account</a></li>
                            <li><a href="#">Add Listing</a></li>
                            <li><a href="#">Pricing</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                        </ul>

                        <ul class="footer-links">
                            <li><a href="#">FAQ</a></li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Our Partners</a></li>
                            <li><a href="#">How It Works</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>

                    <div class="col-md-3  col-sm-12">
                        <h4>Contact Us</h4>
                        <div class="text-widget">
                            <span>12345 Little Lonsdale St, Melbourne</span> <br>
                            Phone: <span>(123) 123-456 </span><br>
                            E-Mail:<span> <a href="#">office@example.com</a> </span><br>
                        </div>

                        <ul class="social-icons margin-top-20">
                            <li><a class="facebook" href="#"><i class="icon-facebook"></i></a></li>
                            <li><a class="twitter" href="#"><i class="icon-twitter"></i></a></li>
                            <li><a class="gplus" href="#"><i class="icon-gplus"></i></a></li>
                            <li><a class="vimeo" href="#"><i class="icon-vimeo"></i></a></li>
                        </ul>

                    </div>

                </div>

                <!-- Copyright -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="copyrights">© 2019 Real Estate Prototype. All Rights Reserved.</div>
                    </div>
                </div>

            </div>

        </div>
        <!-- Footer / End -->

        <!-- Back To Top Button -->
        <div id="backtotop"><a href="#"></a></div>
    </div>
    <!-- Wrapper / End -->



    <!-- Scripts
================================================== -->
    <script type="text/javascript" src="/scripts/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="/scripts/jquery-migrate-3.1.0.min.js"></script>
    <script type="text/javascript" src="/scripts/mmenu.min.js"></script>
    <script type="text/javascript" src="/scripts/chosen.min.js"></script>
    <script type="text/javascript" src="/scripts/slick.min.js"></script>
    <script type="text/javascript" src="/scripts/rangeslider.min.js"></script>
    <script type="text/javascript" src="/scripts/magnific-popup.min.js"></script>
    <script type="text/javascript" src="/scripts/waypoints.min.js"></script>
    <script type="text/javascript" src="/scripts/counterup.min.js"></script>
    <script type="text/javascript" src="/scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/scripts/tooltips.min.js"></script>
    <script type="text/javascript" src="/scripts/custom.js"></script>

    @yield('scripts')

    <!-- Maps -->
    <script
        src="https://maps.googleapis.com/maps/api/js?key=removedkey&amp;libraries=places&amp;callback=initialize">
    </script>
    <script type="text/javascript" src="/scripts/infobox.min.js"></script>
    <script type="text/javascript" src="/scripts/markerclusterer.js"></script>
    {{-- <script type="text/javascript" src="/scripts/maps.js"></script> --}}

    <script>
        $(".chosen-select").chosen({disable_search_threshold: 10});
    </script>



    <!-- Style Switcher
================================================== -->
    <script src="/scripts/switcher.js"></script>

    <div id="style-switcher">
        <h2>Color Switcher <a href="#"><i class="sl sl-icon-settings"></i></a></h2>

        <div>
            <ul class="colors" id="color1">
                <li><a href="#" class="main" title="Main"></a></li>
                <li><a href="#" class="blue" title="Blue"></a></li>
                <li><a href="#" class="green" title="Green"></a></li>
                <li><a href="#" class="orange" title="Orange"></a></li>
                <li><a href="#" class="navy" title="Navy"></a></li>
                <li><a href="#" class="yellow" title="Yellow"></a></li>
                <li><a href="#" class="peach" title="Peach"></a></li>
                <li><a href="#" class="beige" title="Beige"></a></li>
                <li><a href="#" class="purple" title="Purple"></a></li>
                <li><a href="#" class="celadon" title="Celadon"></a></li>
                <li><a href="#" class="red" title="Red"></a></li>
                <li><a href="#" class="brown" title="Brown"></a></li>
                <li><a href="#" class="cherry" title="Cherry"></a></li>
                <li><a href="#" class="cyan" title="Cyan"></a></li>
                <li><a href="#" class="gray" title="Gray"></a></li>
                <li><a href="#" class="olive" title="Olive"></a></li>
            </ul>
        </div>

    </div>
    <!-- Style Switcher / End -->

    <div id="mm-blocker" class="mm-slideout"></div>
</body>
<div id="tiptip_holder" style="max-width:200px;">
    <div id="tiptip_arrow">
        <div id="tiptip_arrow_inner"></div>
    </div>
    <div id="tiptip_content"></div>
</div>

</html>