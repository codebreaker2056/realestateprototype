@extends('layouts.app')

@section('content')
<!-- Map
================================================== -->
<div id="map-container" class="fullwidth-home-map">
    <div id="address-map-container" data-map-zoom="9" style="position: relative; overflow: hidden;">
        <div style="width: 100%; height: 100%" id="address-map"></div>
    </div>

    <div class="main-search-inner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <form action="{{ route('search') }}" method="post" class="main-search-input">
                        
                            {{-- <div class="main-search-input-item">
                            <input type="text" placeholder="What are you looking for?" value="">
                        </div> --}}

                        <div class="main-search-input-item location">
                            <div id="autocomplete-container">
                                <div class="pac-container pac-logo"
                                    style="display: none; width: 270px; position: absolute; left: 469px; top: 613px;">
                                </div>

                                <input id="address-input" type="text" placeholder="Location" name="address_address" class="form-control map-input" autocomplete="off">
                                <input type="hidden" name="address_latitude" id="address-latitude" value="0" />
                                <input type="hidden" name="address_longitude" id="address-longitude" value="0" />
                            </div>
                            <a href="#"><i class="fa fa-map-marker"></i></a>
                        </div>

                        {{-- <div class="main-search-input-item">
                            <select data-placeholder="All Categories" class="chosen-select" style="display: none;">
                                <option>All Categories</option>
                                <option>Shops</option>
                                <option>Hotels</option>
                                <option>Restaurants</option>
                                <option>Fitness</option>
                                <option>Events</option>
                            </select>
                            <div class="chosen-container chosen-container-single chosen-container-single-nosearch" style="width: 100%;" title="">
                                <a class="chosen-single chosen-default"><span>All Categories</span>
                                    <div>
                                        <b></b>
                                    </div>
                                </a>
                                <div class="chosen-drop">
                                    <div class="chosen-search"><input type="text" autocomplete="off" readonly=""></div>
                                    <ul class="chosen-results"></ul>
                                </div>
                            </div>
                        </div> --}}

                        <button type="submit" class="button">Search</button>
                        @csrf
                    </form>
                </div>
            </div>
        </div>

    </div>

    <!-- Scroll Enabling Button -->
    <a href="#" id="scrollEnabling" title="Enable or disable scrolling on map">Enable Scrolling</a>
</div>
@endsection

@section('scripts')

<script src="/js/mapInput.js"></script>
<!-- Google Autocomplete -->
<script>
        function initAutocomplete() {
        var input = document.getElementById('autocomplete-input');
        var autocomplete = new google.maps.places.Autocomplete(input);

        autocomplete.addListener('place_changed', function() {
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            window.alert("No details available for input: '" + place.name + "'");
            return;
        }
        });

        if ($('.main-search-input-item')[0]) {
            setTimeout(function(){ 
                $(".pac-container").prependTo("#autocomplete-container");
            }, 300);
        }
    }
</script>
@endsection