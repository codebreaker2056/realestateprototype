@extends('layouts.dashboard')

@section('content')
<div class="col-lg-12">
    <div id="add-listing">
        <!-- Section -->
        <div class="add-listing-section margin-top-45">
            <!-- Headline -->
            <div class="add-listing-headline">
                <h3><i class="sl sl-icon-location"></i> Location</h3>
            </div>

            <!-- Dropzone -->
            <div id="map-container" class="fullwidth-home-map">
                <div id="address-map-container" data-map-zoom="9" style="position: relative; overflow: hidden;">
                    <div style="width: 100%; height: 100%" id="address-map"></div>
                </div>
            </div>

            <div class="submit-section" style="margin-top: 20px;">
                <!-- Row -->
                <div class="row with-forms">
                    <!-- Address -->
                    <div class="col-md-12">
                        <h5>Property Address</h5>
                        <input id="address-input" type="text" placeholder="Location" name="address_address" class="form-control map-input" autocomplete="off">
                        <input type="hidden" name="address_latitude" id="address-latitude" value="0" />
                        <input type="hidden" name="address_longitude" id="address-longitude" value="0" />
                    </div>

                    <div class="col-md-12">
                        <h5>Property Title <i class="tip" data-tip-content="Name of your business"><div class="tip-content">Name of your business</div></i></h5>
                        <input class="search-field" type="text" value="">
                    </div>

                    <div class="col-md-12">
                        <h5>Describe More About Your Property</h5>
                        <textarea class="WYSIWYG" name="summary" cols="40" rows="2" id="summary" placeholder="Enter any notes here" spellcheck="false"></textarea>
                    </div>
                </div>
                <!-- Row / End -->
            </div>
        </div>
        <!-- Section / End -->

        <a href="/" class="button preview">Post <i class="fa fa-arrow-circle-right"></i></a>

    </div>
</div>
@endsection

@section('scripts')
<script src="/js/mapInput.js"></script>
@endsection