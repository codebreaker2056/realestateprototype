<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'CitiesController@index');
Route::any('/search-cities', 'CitiesController@search')->name('search');

Route::get('/add-listing', 'CitiesController@create')->name('create');

Route::get('/welcome', function () {
    return view('welcome');
});
