<?php

namespace App\Http\Controllers;

use App\City;
use Illuminate\Http\Request;

class CitiesController extends Controller
{
    public function index()
    {
        return view('pageone');
    }

    public function search(Request $request)
    {
        $this->validate($request, [
            'address_address' => 'required',
            'address_latitude' => 'required',
            'address_longitude' => 'required'
        ]);

        $address_address = $request->get('address_address');
        $address_latitude = $request->get('address_latitude');
        $address_longitude = $request->get('address_longitude');

        return view('listing', compact('address_address', 'address_latitude', 'address_longitude'));
    }

    public function create(Request $request)
    {
        return view('pagetwo');
    }

    public function store(Request $request)
    {
        abort_unless(\Gate::allows('company_create'), 403);
        $company = City::create($request->all());
        return redirect()->route('admin.companies.index');
    }
}
